<?php
$data = [
    'server' => [
        'items' => [
            'php' => [
                'label' => 'PHP',
                'services' => [
                    'PHP Basic',
                    'PHP Yii2',
                    'PHP Laravel',
                    'PHP Symfony',
                    'PHP Cake',
                    'PHP Zend'
                ],
            ],
            'python' => [
                'label' => 'Python',
                'services' => [
                    'Python Basic',
                    'Python Django',
                    'Python Flask',
                    'Python Pyramid'
                ]
            ]
        ],
        'label' => 'Server-Side'
    ],
    'client' => [
        'items' => [
            'javascript' => [
                'label' => 'JavaScript',
                'services' => [
                    'Ecmascript 6',
                    'VueJS'
                ]
            ],
            'android' => [
                'label' => 'Android',
                'services' => [
                    'Java Basic',
                    'Android SDK'
                ]
            ],
            'ios' => [
                'label' => 'iOS',
                'services' => [
                    'Swift Basic',
                    'Cocoa Framework'
                ]
            ]
        ],
        'label' => 'Client-Side'
    ]
];

$template = <<<HTML
<h1>Предлагаемые услуги:</h1>
    <ul class="nav nav-tabs" role="tablist">
        {{tab}}
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#tab-{{index}}" role="tab" aria-controls="home" aria-selected="true">{{label}}</a>
            </li>
        {{/tab}}
    </ul>
    <div class="tab-content">
        {{content}}
            <div class="tab-pane fade show active" id="tab-{{index}}" role="tabpanel" aria-labelledby="home-tab">
                {{language}}
                    <div class="card d-inline-block" style="width: 18rem;">
                        <img class="card-img-top" src="https://dummyimage.com/250/000000/ffffff" alt="{{title}}">
                        <div class="card-body">
                            <h5 class="card-title">{{title}}</h5>
                            <p class="card-text">
                            <ul class="list-group list-group-flush">
                                {{services}}
                                    <li class="list-group-item">{{service-title}}</li>
                                {{/services}}
                            </ul>
                            </p>
                            <a href="#" class="btn btn-primary">Записаться</a>
                        </div>
                    </div>
                {{/language}}
            </div>
        {{/content}}
    </div>
HTML
;

/**
 * @param string $template кусок шаблона СОДЕРЖИМОЕ ТЕГОВ {{tab}}{{/tab}}
 * @param array $items элементы которые я буду выводить вместо содержимого
 */
function createTabs($template, $items)
{

    $output = "";
    foreach($items as $key => $label){
        $output .= str_replace(['{{index}}','{{label}}'], [$key, $label], $template);
    }
    return $output;
}

/**
 * @param string $template кусок шаблона СОДЕРЖИМОЕ ТЕГОВ {{content}}{{/content}}
 * @param array $items массив дисциплин
 */
function createContent($template, $items)
{
    $output = '';
    foreach($items as $key => $side){
        $content = str_replace('{{index}}', $key, $template);
        preg_match('#\{\{language\}\}(.*?)\{\{\/language\}\}#s', $content, $params);
        createLangs($params[1], $side['items']);
    }
}


/**
 * @param string $template кусок шаблона СОДЕРЖИМОЕ ТЕГОВ {{language}}{{/language}}
 * @param array $items массив языков сервера или клиента
 */
function createLangs($template, $items)
{
    foreach($items as $lang){
        $content = str_replace('{{title}}', $lang['label'], $template);
    }
}



preg_match('#\{\{tab\}\}(.*?)\{\{\/tab\}\}#s', $template, $params);
$tabs = createTabs($params[1], array_column($data, 'label'));


preg_match('#\{\{content\}\}(.*?)\{\{\/content\}\}#s', $template, $params);
$content = createContent($params[1], $data);
