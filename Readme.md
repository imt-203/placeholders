# Вывод категорий услуг и конкретных услуг в табах

## Описание задачи

Есть приложение (сайт) компании, которая предлагает определенные услуги, в нашем примере, это будет IT-академия, которая предлагает услуги по обучению на следующие дисциплины: Server-Side: PHP, Python. Client-Side: JavaScript, Andoid, iOS. Дисциплины разделены между собой по категориям: Server-Side и Client-Side, кроме того, каждый предмет (PHP, Python, JavaScript, etc..), так же делится на семестры (PHP Basic, Yii2, etc...). Все эти услуги хранятся в каком либо хранилище (обычно это база данных, но в нашем случае это простой массив PHP). 

Что бы вывести информацию о предоставляемых услугах компании, обычно используют перебор массива встраивая PHP в HTML. Структуру HTML обычно предоставляет "верстальщик". Но что если заказчик, сам является верстальщиком, который может (и хочет) радикально менять HTML верстку (структуру), но при этом не желает просматривать исходники проекта в поисках нужного файла, кроме того, он не хочет разбираться с PHP? Было принято решение использовать "плейсхолдеры", вместо выражений PHP. А сама "верстка" будет хранится в хранилище, примерно так как обычно хранят статьи в блоге и тому подобные вещи. Плейсхолдеры напоминают bb-коды на форумах, или emoji. Пользователь редатирует контент и может вставлять какие-то "специальные вещи", которые в последствии будут заменены на то, что необходимо, средствами PHP. 

Первоначальная верстка подразумевает вывод в виде табов, где Label каждого таба - название категории, а содержимое - карточка с названием и перечислением семестров. Что бы верстку можно было менять как угодно, но при этом контролировать вывод динамических данных, было принято решение использовать "плейсхолдеры". Плейсхолдер - это согласованная строка, которая означает вывод или начало операции. По таком принципу работают такие шаблонизаторы как Smarty, Twig и Blade. Они могут иметь любой вид, например: ```%username%```, в данном случае подразумевается вывод имени пользователя. Или может иметь вид такой: ```{{username}}``` - так же выведет имя пользователя. Мы, разработчики, можем использовать любые символы, какие захотим, это лишь соглашение между разработчиком и верстальщиком, или контент менеджером. 

> Имена "плейсхолдеров" обычно задает разработчик, а верстальщик может их использовать и вставлять там, где считает нужным.

## Описание "плейсхолдеров"

| Placeholder  | Описание  |
|:------------- |:---------------:|
| ```{{tab}}``` | Начало цикла для вывода заголовка табов |
| ```{{/tab}}```| Конец цикла для вывода табов  |
| ```{{label}}```| Заголовок категории  |
| ```{{index}}```| Индекс перебора цикла        |
| ```{{content}}```| Начало цикла для вывода контента табов|
| ```{{/content}}```| Конец цикла для вывода контента табов|
| ```{{title}}```| Заголовок дисциплины |
| ```{{services}}```| Начало цикла вывода семестров |
| ```{{/services}}```| Конец цикла вывода семестров |
| ```{{service-title}}```| Заголовок сервиса |

> Верстка находится в переменной ```$template```

### Задача

Используя данные в массиве ```$data``` и шаблон в переменной ```$template``` которые находятся в файле [store.php](store.php "store.php") организовать вывод данных, так как это выводится на странице [Demo](demo.php "Demo"), но использовать средства PHP. Другими словами, нужно "распарсить" шаблон и вставить вместо "плейсхолдеров" данные из массива ```$data```.