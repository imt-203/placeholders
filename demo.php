<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <div class="container-fluid">
        <h1>Предлагаемые услуги:</h1>
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" 
                data-toggle="tab" href="#tab-1" role="tab" aria-controls="home" aria-selected="true">Server-Side</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" 
                data-toggle="tab" href="#tab-2" role="tab" aria-controls="profile" 
                aria-selected="false">Client-Side</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="tab-1" role="tabpanel" aria-labelledby="home-tab">
                <div class="card d-inline-block" style="width: 18rem;">
                    <img class="card-img-top" src="https://dummyimage.com/250/000000/ffffff" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">PHP</h5>
                        <p class="card-text">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">PHP Basic</li>
                            <li class="list-group-item">PHP Yii2</li>
                            <li class="list-group-item">PHP Laravel</li>
                            <li class="list-group-item">PHP Symfony</li>
                        </ul>
                        </p>
                        <a href="#" class="btn btn-primary">Записаться</a>
                    </div>
                </div>
                <div class="card d-inline-block" style="width: 18rem;">
                    <img class="card-img-top" src="https://dummyimage.com/250/000000/ffffff" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Python</h5>
                        <p class="card-text">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Python Basic</li>
                            <li class="list-group-item">PHP Django</li>
                            <li class="list-group-item">PHP Flask</li>
                            <li class="list-group-item">PHP Pyramid</li>
                        </ul>
                        </p>
                        <a href="#" class="btn btn-primary">Записаться</a>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tab-2" role="tabpanel" aria-labelledby="home-tab">
                <div class="card d-inline-block" style="width: 18rem;">
                    <img class="card-img-top" src="https://dummyimage.com/250/000000/ffffff" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">JavaScript</h5>
                        <p class="card-text">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Ecmascript 6</li>
                            <li class="list-group-item">VueJS</li>
                        </ul>
                        </p>
                        <a href="#" class="btn btn-primary">Записаться</a>
                    </div>
                </div>
                <div class="card d-inline-block" style="width: 18rem;">
                    <img class="card-img-top" src="https://dummyimage.com/250/000000/ffffff" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Android</h5>
                        <p class="card-text">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Java Basic</li>
                            <li class="list-group-item">Android SDK</li>
                        </ul>
                        </p>
                        <a href="#" class="btn btn-primary">Записаться</a>
                    </div>
                </div>
                <div class="card d-inline-block" style="width: 18rem;">
                    <img class="card-img-top" src="https://dummyimage.com/250/000000/ffffff" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">iOS</h5>
                        <p class="card-text">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Swift Basic</li>
                            <li class="list-group-item">Cocoa Framework</li>
                        </ul>
                        </p>
                        <a href="#" class="btn btn-primary">Записаться</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>